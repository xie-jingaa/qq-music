//封装横向滚动的方法，方便后续调用
import BScroll from "better-scroll";

/**
 * 
 * @param {*} that 代表当前实例
 * @param {*} wrapper 代表滚动的容器
 * @param {*} content 代表滚动的内容区
 * @param {*} num 代表滚动项的数目
 * @param {*} itemwidth 代表滚动项的宽度
 */

let betterScrollHorizontal = function (that, wrapper, content, num, itemwidth) {
    // 动态获取到内容区content的宽度
    content.style.width = num * itemwidth + "rem";
    //异步数据的一些处理
    that.$nextTick(() => {
        that.scroll = new BScroll(wrapper, {
            // wrapper为template中容器的ref，表示给那个元素设置滚动
            scrollY: false, //关闭纵向横动
            scrollX: true, //横向的滚动
            startX: 0, //横向滚动的开始位置
            click: true,//开启浏览器的原生click事件
        });
    });
}
export { betterScrollHorizontal };