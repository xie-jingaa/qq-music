import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
   singer:{
   
   }
  },
  mutations: {
    saveSinger(state,singer){
        state.singer=singer
    }
  },
  actions:{
    
  }
})
export default store