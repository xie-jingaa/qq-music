/**
 * 路由文件，负责处理路由
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import SingList from '../components/songlist/SongList.vue'
import SingerHome from '../components/singer/SingerHome.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },//主页home
  {
    path: '/about',
    name: 'about',
    component: AboutView
  },//关于页面
  {
    path: '/singlist/:id',//：id为路由需要传递的动态参数
    name: 'singlist',
    component: SingList
  },//歌单详情页
  {
    path: '/singerhome/:id',//：id为路由需要传递的动态参数
    name: 'singerhome',
    component: SingerHome
  }//歌手详情页
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
