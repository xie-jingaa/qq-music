import Vue from 'vue'
import App from './App.vue'
import router from './router'
import request from './common/request.js'
import store from './store'
import { Tabbar, TabbarItem, Search } from 'vant'
import { Swipe, SwipeItem } from 'vant';
import { Image as VanImage, Lazyload } from 'vant';
import { NavBar, List } from 'vant';
import { Tab, Tabs } from 'vant';
// import Vuex from 'vuex'

// Vue.use(Vuex)
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(List);
Vue.use(NavBar);
Vue.use(VanImage);
Vue.use(Lazyload);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Search);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.config.productionTip = false
Vue.prototype.$request = request.httpRequest

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
